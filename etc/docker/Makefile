# Makefile for builing, pushing and pulling images.
#
# The build can be influence by setting a number of environment variables:
#   IMAGE_TAG			What to tag to set when building. Default "latest"
#   IMAGE_PREFIX				What to IMAGE_PREFIX image names with when building. Default name of local git repository (the directory name)
#   REGISTRY			Which registry to push/pull images from. Default "XXX"
#   REMOTE_IMAGE_PREFIX		What to IMAGE_PREFIX image names with for push/pull. Default git remote origin url
#   REMOTE_IMAGE_TAG	Which tag to use to pull and push. Default $IMAGE_TAG
#
export IMAGE_TAG ?= latest
export IMAGE_PREFIX ?= $(shell basename $(realpath $(CURDIR)/../..))/
export REGISTRY ?= registry.gitlab.com
export REMOTE_IMAGE_PREFIX ?= $(REGISTRY)/$(shell git config --get remote.origin.url | sed 's|^git@[a-zA-Z.]*:||; s|^https://[^/]*/||; s|\.git$$||')/
export REMOTE_IMAGE_TAG ?= $(IMAGE_TAG)
export CONTEXT = $(CURDIR)/../..
export DOCKER_BUILDKIT = 1
comma := ,
subdirs = $(shell echo */ | xargs -n 1 basename)
buildx-images = $(addprefix buildx-,$(subdirs))

.PHONY: $(subdirs)

buildx: base confbox app $(buildx-images)
base: buildx-base
confbox: base buildx-confbox
app: confbox buildx-app
oradb: buildx-oradb

# target for building an image
build-%: pull-%-cache
	$(eval IMAGE_ID := $(IMAGE_PREFIX)$*:$(IMAGE_TAG))
	@echo === Build $(IMAGE_ID)
	docker build --build-arg IMAGE_PREFIX=$(IMAGE_PREFIX) --build-arg IMAGE_TAG=$(IMAGE_TAG) --label IMAGE_PREFIX=$(IMAGE_PREFIX) $(CACHE_FROM_OPTS) -t $(IMAGE_ID) --load -f $(CURDIR)/$*/Dockerfile $(CONTEXT); \

buildx-%:
	$(eval IMAGE_ID := $(IMAGE_PREFIX)$*:$(IMAGE_TAG))
	@echo === Buildx $(IMAGE_ID)
	docker buildx inspect buildkit-builder || docker buildx create --driver=docker-container --name=buildkit-builder --use

	$(eval export CACHE_FROM = $(addprefix  $(REMOTE_IMAGE_PREFIX)$*:,$(shell git rev-parse HEAD~1) latest_cache latest cache))
	$(eval export CACHE_FROM_OPTS = $(addprefix --cache-from type=registry$(comma)ref=,$(CACHE_FROM)))
	$(eval export CACHE_TO_OPTS = --cache-to type=registry,ref=$(REMOTE_IMAGE_PREFIX)$*:cache)
	docker buildx build --build-arg IMAGE_PREFIX=$(IMAGE_PREFIX) --build-arg IMAGE_TAG=$(IMAGE_TAG) --label IMAGE_PREFIX=$(IMAGE_PREFIX) $(CACHE_FROM_OPTS) $(CACHE_TO_OPTS) -t $(IMAGE_ID) --load -f $(CURDIR)/$*/Dockerfile $(CONTEXT); \

# target for pulling a suitable image for caching
pull-%-cache:
	$(eval cache = $(shell docker images --filter=reference="$(IMAGE_PREFIX)$*" --filter=reference="$(REMOTE_IMAGE_PREFIX)$*" --format "{{.Repository}}:{{.Tag}}"))
	$(eval export CACHE_FROM = $(addprefix  $(REMOTE_IMAGE_PREFIX)$*:,$(shell git rev-parse HEAD~1) latest_cache latest cache))
	@if [ -z "$(cache)" ]; then \
		echo "Trying to pull cached image for building $* ..."; \
		for cache_tag in	$(shell git rev-parse HEAD~1) \
							latest_cache \
							latest; do \
			if docker pull $(REMOTE_IMAGE_PREFIX)$*:$${cache_tag} 2>/dev/null 1>&2; then \
				echo "Pulled $(REMOTE_IMAGE_PREFIX)$*:$${cache_tag} for caching" ; \
				break ; \
			fi \
		done \
	else \
		echo "Using existing cache for $*: $(cache)"; \
	fi
	$(eval export CACHE_FROM_OPTS = $(addprefix --cache-from type=registry$(comma)ref=,$(CACHE_FROM)))

# list the full name of an image
ls: $(addprefix ls-,$(subdirs))
ls-%:
	@echo $(IMAGE_PREFIX)$*:$(IMAGE_TAG)
lsr: $(addprefix lsr-,$(subdirs))
lsr-%:
	@echo $(REMOTE_IMAGE_PREFIX)$*:$(REMOTE_IMAGE_TAG)

# pull all images from the reigistry
pull: $(addprefix pull-,$(subdirs))

# pull a single image from the registry and tag it with the local IMAGE_PREFIX
pull-%:
	$(eval IMAGE_ID := $(IMAGE_PREFIX)$*:$(IMAGE_TAG))
	$(eval REMOTE_IMAGE_ID := $(REMOTE_IMAGE_PREFIX)$*:$(REMOTE_IMAGE_TAG))
	@echo === Pull $(REMOTE_IMAGE_ID)
	docker pull $(REMOTE_IMAGE_ID)
	docker tag $(REMOTE_IMAGE_ID) $(IMAGE_ID)

# push all images to the registry
push: $(addprefix push-,$(subdirs))

# tag an image with the remote tag and push it
push-%:
	$(eval IMAGE_ID := $(IMAGE_PREFIX)$*:$(IMAGE_TAG))
	$(eval REMOTE_IMAGE_ID := $(REMOTE_IMAGE_PREFIX)$*:$(REMOTE_IMAGE_TAG))
	@echo === Push $(REMOTE_IMAGE_ID)
	docker tag $(IMAGE_ID) $(REMOTE_IMAGE_ID)
	docker push -q $(REMOTE_IMAGE_ID)

# remove all local and remote tags
clean: clean-local clean-remote
clean-local:
	[ ! -z "$$(docker images -f label=IMAGE_PREFIX=$(IMAGE_PREFIX) -q)" ] && docker rmi $$(docker images -f label=IMAGE_PREFIX=$(IMAGE_PREFIX) -q)
clean-remote:
	[ ! -z "$$(docker images -f label=IMAGE_PREFIX=$(REMOTE_IMAGE_PREFIX) -q)" ] && docker rmi $$(docker images -f label=IMAGE_PREFIX=$(REMOTE_IMAGE_PREFIX) -q)
clean-volumes:
	$(eval COMPOSE_PROJECT := $(shell basename $(realpath $(CURDIR)/../..)))
	docker volume prune -f --filter label=com.docker.compose.project=$(COMPOSE_PROJECT)
